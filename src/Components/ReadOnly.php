<?php

namespace Marcarian\LaravelModelFormBuilder\Components;

class ReadOnly extends BaseComponent {

    protected $view = 'readonly';

    public function __construct(array $attributes)
    {
        $this->build($attributes);
    }


}