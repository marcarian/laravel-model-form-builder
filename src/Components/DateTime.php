<?php

namespace Marcarian\LaravelModelFormBuilder\Components;

class DateTime extends BaseComponent {

    protected $view = 'datetime';

    public function __construct(array $attributes)
    {
        // Make sure the date format is formated according to given standard.
        $attributes['value'] = date(config('form-builder.datetime_format'), strtotime($attributes['value']));

        parent::__construct($attributes);
    }

}