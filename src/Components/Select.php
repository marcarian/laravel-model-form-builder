<?php

namespace Marcarian\LaravelModelFormBuilder\Components;

class Select extends BaseComponent {

    protected $options = [];
    protected $view = 'select';

    public function setOptions($options = [])
    {
        $this->options = $options;
    }

}