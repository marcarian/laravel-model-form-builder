<?php

namespace Marcarian\LaravelModelFormBuilder\Components;

class Date extends BaseComponent {

    protected $view = 'date';

    public function __construct(array $attributes)
    {
        // Make sure the date format is formated according to given standard.
        $attributes['value'] = date(config('form-builder.date_format'), strtotime($attributes['value']));

        parent::__construct($attributes);
    }

}