<?php

namespace Marcarian\LaravelModelFormBuilder\Components;

interface ComponentInterface {

    public function build(array $attributes);

}