<?php

namespace Marcarian\LaravelModelFormBuilder\Components;

use Marcarian\LaravelModelFormBuilder\Components\Traits\Buildable;
use Marcarian\LaravelModelFormBuilder\Traits\HasAttributes;

class Submit extends BaseComponent {

    protected $view = 'submit';

}