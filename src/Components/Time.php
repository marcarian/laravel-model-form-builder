<?php

namespace Marcarian\LaravelModelFormBuilder\Components;

class Time extends BaseComponent {

    protected $view = 'time';

    public function __construct(array $attributes)
    {
        // Make sure the date format is formated according to given standard.
        $attributes['value'] = date(config('form-builder.time_format'), strtotime($attributes['value']));

        parent::__construct($attributes);
    }
}