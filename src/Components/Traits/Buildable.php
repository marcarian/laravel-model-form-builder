<?php

namespace Marcarian\LaravelModelFormBuilder\Components\Traits;

trait Buildable {

    public function build(array $attributes)
    {
        foreach($attributes as $key => $attribute) {
            $this->setAttribute($key, $attribute);
        }
    }

}