<?php

namespace Marcarian\LaravelModelFormBuilder\Components;


use Marcarian\LaravelModelFormBuilder\Components\Traits\Buildable;
use Marcarian\LaravelModelFormBuilder\Traits\HasAttributes;

abstract class BaseComponent implements ComponentInterface {

    use Buildable, HasAttributes;

    protected $casts = [];

    public function __construct(array $attributes)
    {
        $this->build($attributes);
    }

    /**
     * @return string
     */
    public function render($title = '', $mode = 'edit')
    {
        return view('form-builder::' . $mode . '.' . $this->view)
            ->with('component', $this)
            ->with('title', $title)
            ->with('mode', $mode)
            ->render();
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return ! is_null($this->getAttribute($key));
    }

    /**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->$method(...$parameters);
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }
}