<div class="row form-builder-row two-ten-md">
	<div class="col-md-2">
		<label>{{ $label }}:</label>
	</div>
	<div class="col-md-10">
		{{ $slot }}
	</div>
</div>