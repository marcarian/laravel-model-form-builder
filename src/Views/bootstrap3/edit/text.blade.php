@component('form-builder::grid-components.two-ten-md')
	@slot('label')
		{{ $component->label }}
	@endslot

	<input type="text" class="form-control" id="exampleInputEmail1" placeholder="{{ $title }}" value="{{ $component->value }}">
@endcomponent