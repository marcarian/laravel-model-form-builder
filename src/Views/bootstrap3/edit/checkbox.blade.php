@component('form-builder::grid-components.two-ten-md')
	@slot('label')
		{{ $component->label }}
	@endslot

	<input type="checkbox" @selected($component->value)>
@endcomponent