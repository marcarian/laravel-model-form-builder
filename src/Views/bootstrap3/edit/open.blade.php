@if($mode === 'edit')
	<form action="{{ $component->action }}">
	{{ method_field('PATCH') }}
@endif