@component('form-builder::grid-components.two-ten-md')
	@slot('label')
		{{ $component->label }}
	@endslot

	<input type="time" class="form-control" placeholder="{{ $component->label }}" value="{{ $component->value }}">
@endcomponent