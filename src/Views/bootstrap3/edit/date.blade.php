@component('form-builder::grid-components.two-ten-md')
	@slot('label')
		{{ $component->label }}
	@endslot

	<input type="date" class="form-control" placeholder="{{ $component->label }}" value="{{ $component->value }}">
@endcomponent