@component('form-builder::grid-components.two-ten-md')
	@slot('label')
		{{ $component->label }}
	@endslot

	<select class="form-control">
		@foreach($component->options as $key => $value)
			<option value="{{ $key }}" @if($component->value === $key) selected @endif>{{ $value }}</option>
		@endforeach
	</select>
@endcomponent