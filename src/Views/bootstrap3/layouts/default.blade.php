<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">{{ $name }}</h3>
	</div>
	<div class="panel-body">
		@foreach($components as $name => $component)
			{!! $component->render($name, $mode) !!}
		@endforeach
	</div>
</div>