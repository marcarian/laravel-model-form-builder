@component('form-builder::grid-components.two-ten-md')
	@slot('label')
		{{ $component->label }}
	@endslot

	@if($component->value)
		<i class="fa fa-check-square-o" aria-hidden="true"></i>
	@else
		<i class="fa fa-square-o" aria-hidden="true"></i>
	@endif
@endcomponent