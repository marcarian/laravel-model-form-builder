@component('form-builder::grid-components.two-ten-md')
	@slot('label')
		{{ $component->label }}
	@endslot

	{!! $component->options[$component->value] !!}
@endcomponent