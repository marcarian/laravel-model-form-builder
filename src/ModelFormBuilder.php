<?php

namespace Marcarian\LaravelModelFormBuilder;


use Carbon\Carbon;
use Marcarian\LaravelModelFormBuilder\Components\Checkbox;
use Marcarian\LaravelModelFormBuilder\Components\DateTime;
use Marcarian\LaravelModelFormBuilder\Components\ReadOnly;
use Marcarian\LaravelModelFormBuilder\Components\Select;
use Marcarian\LaravelModelFormBuilder\Components\Text;
use Marcarian\LaravelModelFormBuilder\Traits\HasAttributes;

class ModelFormBuilder
{
    use HasAttributes;

    protected $model;
    protected $options;
    protected $components = [];
    protected $casts = [
        'id' => ReadOnly::class,
        'created_at' => ReadOnly::class,
        'updated_at' => ReadOnly::class,
        'deleted_at' => ReadOnly::class
    ];

    private $formAction;
    private $mode;


    public function __construct($model, $mode = 'edit')
    {
        $this->model = $model;
        $this->setMode($mode);
        $this->options = $this->getRelatedOptions();
        $this->setFormAction($this->extractFormActionFromModel());
        $this->addCasts();
    }


    public function build()
    {
        $this->cast();
        if ($this->isInEditMode()) {
            $this->addComponent('open', 'Open', ['action' => $this->getFormAction()]);
        }
        $this->addModelComponents();
        $this->addComponent('submit', 'Submit');
        $this->addComponent('close', 'Close');
    }

    /**
     * @param string $attributeName
     * @param string $component
     * @param array $data
     */
    public function addComponent(string $attributeName, string $component, array $data = [])
    {
        $component = $this->getComponentClass($component);
        $this->setAttribute($attributeName, new $component($data));
    }

    public function addModelComponents()
    {
        collect($this->model->getAttributes())
            ->reject(function($attribute, $key) {
                return in_array($key, $this->model->getHidden());
            })
            ->each(function($attribute, $key) {
                $component = new $this->casts[$key](['value' => $attribute]);
                $this->setAttribute($key, $component);
                $component->setAttribute('label', $this->getLabelFromKey($key));
                if ($component instanceof Select) {
                    $index = $this->getRelatedPrimaries()->search($key);
                    $component->setAttribute('options', $this->options[$index]);
                }
            });
    }

    /**
     * @param string $component
     * @return string
     */
    private function getComponentClass(string $component): string
    {
        return __NAMESPACE__ . '\Components\\' . $component;
    }

    public function addCasts($casts = [])
    {
        $this->casts = collect($this->casts)->merge($casts);

        return $this;
    }

    public function cast()
    {
        $casts = [];
        foreach($this->model->getAttributes() as $key => $value) {
            if (!$this->casts->has($key)) {
                switch (gettype($value)) {
                    case 'boolean':
                        $casts[$key] = Checkbox::class;
                        break;
                    case 'integer':
                        if ($this->getRelatedPrimaries()->contains($key)) {
                            $casts[$key] = Select::class;
                        } else {
                            $casts[$key] = Text::class;
                        }
                        break;
                    default:
                        if (date ($this->getDateFormat(), strtotime($value)) === $value) {
                            $casts[$key] = DateTime::class;
                        } else {
                            $casts[$key] = Text::class;
                        }
                        break;
                }
            }
        }

        $this->casts = collect($this->casts)->merge($casts);
    }

    private function getRelatedPrimaries()
    {
        return collect($this->model->getRelations())
            ->map(function($relation) {
                $exploded = explode('.', $relation->getTable());
                return str_singular(end($exploded)) . '_id';
            });
    }

    private function getRelatedOptions()
    {
        return collect($this->model->getRelations())
            ->map(function($relation, $key) {
                return $relation::get()->pluck($this->getOptionAttributeName($key), 'id');
            })
            ->toArray();
    }

    public function render()
    {
        return view('form-builder::layouts.default')
            ->with('components', $this->getAttributes())
            ->with('formAction', $this->getFormAction())
            ->with('mode', $this->getMode())
            ->with('name', $this->getName())
            ->render();
    }

    public function getName()
    {
        return class_basename(get_class($this->model));
    }

    private function getOptionAttributeName($key): string
    {
        $optionAttributes = config('form-builder.relation_column_options');

        if (array_key_exists($key, $optionAttributes)) {
            $optionAttribute = $optionAttributes[$key];
        } else {
            $optionAttribute = 'name';
        }

        return $optionAttribute;
    }

    private function getLabelFromKey(string $key): string
    {
        $labels = config('form-builder.labels');

        if (array_key_exists($key, $labels)) {
            $label = str_replace('_', ' ', $labels[$key]);
        } else {
            $label = title_case($key);
            $label = str_replace(['_Id', '_'], ['', ' '], $label);
        }

        return $label;
    }

    /**
     * @return string
     */
    public function extractFormActionFromModel()
    {
        return '/' . strtolower(str_plural(class_basename($this->model))) . '/' . $this->model->id;
    }

    /**
     * @param $formAction
     *
     * @return $this
     */
    public function setFormAction($formAction)
    {
        $this->formAction = $formAction;

        return $this;
    }

    public function getFormAction()
    {
        return $this->formAction;
    }


    public function getMode()
    {
        return $this->mode;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    public function isInEditMode()
    {
        return $this->mode === 'edit';
    }
}
