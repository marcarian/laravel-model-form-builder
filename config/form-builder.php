<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Marcarian Form Builder Settings
     |--------------------------------------------------------------------------
     |
     | This is the Marcarian Form Builder settings file.
     |
     */

    'css_framework' => 'bootstrap3',

    'datetime_format' => 'Y-m-d H:i:s',
    'date_format' => 'Y-m-d',
    'time_format' => 'H:i:s',

    'labels' => [
        'id' => 'ID',
        'name' => 'Namn',
        'active' => 'Aktiv',
        'created_at' => 'Skapad',
        'updated_at' => 'Uppdaterad',
        'deleted_at' => 'Raderad',
    ],

    'relation_column_options' => [
        'locale' => 'iso',
    ],

    'hidden' => [
        'deleted_at'
    ],

    'date_unknown_replacement' => 'Unknown',

];
