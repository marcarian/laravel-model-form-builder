<?php
namespace Marcarian\Core\Test;

use Illuminate\Support\Collection;

class SortAndFilterAssocTest extends TestCase
{
    /** @test */
    public function it_provides_a_sort_and_filter_assoc_macro()
    {
        $this->assertTrue(Collection::hasMacro('sortAndFilterAssoc'));
    }
}